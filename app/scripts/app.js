$(document).ready(function(){
   $('.vr-panel-toggle').on('click', function(evt){
       evt.preventDefault();
        var $link = $(this);
        var $i = $('i', $link);
        var $panelBody = $link.closest('.vr-panel').find('.vr-panel-body');
        if ($i.hasClass('fa-angle-up')){
            $i.removeClass('fa-angle-up').addClass('fa-angle-down');
            $panelBody.slideUp(500);
        }
       else{
           $i.removeClass('fa-angle-down').addClass('fa-angle-up');
            $panelBody.slideDown(500);
       }

   });

   $('.vr-tab-header a').click(function(evt){
       evt.preventDefault();
       var $selectedTab = $(this);
       if($selectedTab.hasClass('selected')){ return false;}

       var $prevTab = $selectedTab.siblings('a.tab-selector').first();
       $prevTab.removeClass('selected');
       $selectedTab.addClass('selected');

       $nextTab = $('#'+$selectedTab.attr('data-tab'));
       $tab = $('#'+$prevTab.attr('data-tab'));
       $tab.hide();
       $nextTab.show();
   })
});