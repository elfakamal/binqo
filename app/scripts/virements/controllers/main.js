'use strict';

/**
 * @ngdoc function
 * @name banqApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the banqApp
 */
angular.module('banqApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [];
  });
