'use strict';

/**
 * @ngdoc overview
 * @name banqApp
 * @description
 * # banqApp
 *
 * Main module of the application.
 */
angular.module('banqApp', [
  'ngAnimate',

  'mgcrea.ngStrap',
  'mgcrea.ngStrap.helpers.dateParser',
  'mgcrea.ngStrap.helpers.dimensions',
  'mgcrea.ngStrap.tooltip'
])
