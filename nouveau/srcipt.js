  var elems = document.querySelectorAll('.vr-checkbox');
    for (var i = 0; i < elems.length; i++) {
        var init = new Switchery(elems[i], {
            color: 'darkblue',
            size: 'small',
            secondaryColor: '#4d4d4f'
        });
    }


     var $date = $('.vr-datepicker').datepicker(
        {
            'language': 'fr',
            'weekStart':1,
            'orientation': 'bottom right',
            'autoclose': true,
            'templates': {
                'leftArrow': '<div class="vr-cercle-datepicker"><i class="glyphicon glyphicon-menu-left"></i></div>',
                'rightArrow': '<div class="vr-cercle-datepicker"><i class="glyphicon glyphicon-menu-right"></i></div>'
            }
        });

    $('.vr-calendar-datepicker').on('click', function (evt) {
        evt.preventDefault();
        $('.vr-datepicker').datepicker('show');
    })